package com.shawnpaulhill.ksql.udf;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class FlattenEventsUdtfTest {
    private static final FlattenEventsUdtf FLATTEN_EVENTS_UDTF = new FlattenEventsUdtf();

    @Test
    void flattenZeroEventsShouldReturnEmptyList() throws JsonProcessingException {
        final List<String> expectedOutput = Collections.emptyList();

        final String input = "<Events></Events>";

        assertEquals(expectedOutput, FLATTEN_EVENTS_UDTF.flattenEvents(input));
    }

    @Test
    void flattenOneEventShouldReturnOneEvent() throws JsonProcessingException {
        final List<String> expectedOutput = Collections.singletonList(
                "<Event Id=\"1254\"><Title>Snow</Title><Probability>0.2</Probability></Event>");

        final String input =
                "<Events>\n" +
                        "<Event Id=\"1254\"><Title>Snow</Title><Probability>0.2</Probability></Event>\n" +
                        "</Events>";

        assertEquals(expectedOutput, FLATTEN_EVENTS_UDTF.flattenEvents(input));
    }

    @Test
    void flattenTwoEventsShouldReturnTwoEvents() throws JsonProcessingException {
        final List<String> expectedOutput = Arrays.asList(
                "<Event Id=\"4578\"><Title>Rain</Title><Probability>0.7</Probability></Event>",
                "<Event Id=\"1254\"><Title>Snow</Title><Probability>0.2</Probability></Event>");

        final String input =
                "<Events>\n" +
                        "    <Event Id=\"4578\"><Title>Rain</Title><Probability>0.7</Probability></Event>\n" +
                        "    <Event Id=\"1254\"><Title>Snow</Title><Probability>0.2</Probability></Event>\n" +
                        "</Events>";

        assertEquals(expectedOutput, FLATTEN_EVENTS_UDTF.flattenEvents(input));
    }

    @Test
    void flattenInvalidXmlShouldThrowException() throws JsonProcessingException {
        assertThrows(JsonParseException.class, () -> FLATTEN_EVENTS_UDTF.flattenEvents("foobar"));
    }

    @Test
    void flattenEmptyStringShouldThrowException() throws JsonProcessingException {
        assertThrows(JsonParseException.class, () -> FLATTEN_EVENTS_UDTF.flattenEvents(""));
    }
}