package com.shawnpaulhill.ksql.udf;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import io.confluent.ksql.function.udf.UdfParameter;
import io.confluent.ksql.function.udtf.Udtf;
import io.confluent.ksql.function.udtf.UdtfDescription;

import java.util.ArrayList;
import java.util.List;

@UdtfDescription(name = "flatten_events", description = "Disassembles a multi-event message into multiple messages each containing a single event")
public class FlattenEventsUdtf {
    private static final XmlMapper xmlMapper = new XmlMapper();

    @Udtf(description = "Takes a String of XML containing a collection of events and returns a collection of strings, each containing a single event.")
    public List<String> flattenEvents(@UdfParameter final String events) throws JsonProcessingException {
        final ArrayList<String> result = new ArrayList<>();
        for (final Event event : xmlMapper.readValue(events, Events.class).events) {
            result.add(xmlMapper.writeValueAsString(event));
        }
        return result;
    }
}