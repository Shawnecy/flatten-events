package com.shawnpaulhill.ksql.udf;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Event {
    @JacksonXmlProperty(localName = "Id", isAttribute = true)
    public final String id;
    @JacksonXmlProperty(localName = "Title")
    public final String title;
    @JacksonXmlProperty(localName = "Probability")
    public final float probability;

    public Event(
            @JacksonXmlProperty(localName = "Id", isAttribute = true)
            final String id,
            @JacksonXmlProperty(localName = "Title")
            final String title,
            @JacksonXmlProperty(localName = "Probability")
            final float probability) {
        this.id = id;
        this.title = title;
        this.probability = probability;
    }
}