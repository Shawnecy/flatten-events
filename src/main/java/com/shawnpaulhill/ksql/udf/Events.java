package com.shawnpaulhill.ksql.udf;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.ArrayList;
import java.util.List;

public class Events {
    public final List<Event> events;

    public Events(
            @JacksonXmlProperty(localName = "Event")
            @JacksonXmlElementWrapper(useWrapping = false)
            final List<Event> events) {
        this.events = events;
    }

    public Events() {
        this.events = new ArrayList<>();
    }
}